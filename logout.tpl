<div class="container">
  <div class="row">
    <div class="col-md-6">

{include file="$template/pageheader.tpl" title=$LANG.logouttitle}

<div class="alert alert-success">
  {$LANG.logoutsuccessful}
</div>

<a href="clientarea.php"><strong>{$LANG.logoutcontinuetext}</strong></a>

		</div>
		<div class="col-md-6">
		</div>
	</div>
</div>